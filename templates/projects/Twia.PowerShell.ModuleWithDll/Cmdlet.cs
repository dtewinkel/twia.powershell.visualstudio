﻿using System;
using System.Management.Automation;

namespace $safeprojectname$
{
    [Cmdlet($verb.code$, "$noun$")]
    public class $verb$$noun$ : PSCmdlet
    {
        protected override void ProcessRecord()
        {
        }
    }
}
