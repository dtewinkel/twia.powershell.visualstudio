# Get information from the build server.
. BuildScripts\GetBuildInfo.ps1

Write-Host "Creating artifact package for '$Product' version $Version."

$artifactDir = mkdir Output -Force
$visxDir = mkdir Output\vsix -Force

$item = Copy-Item deployments\Twia.PowerShell.Vsix\bin\$($Configuration)\Twia.PowerShell.vsix -Destination $visxDir
$item = Copy-Item deployments\Twia.PowerShell.Vsix\Description.md -Destination $visxDir
$item = Copy-Item ReadMe.html -Destination $artifactDir
