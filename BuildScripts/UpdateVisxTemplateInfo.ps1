function UpdateVisxTemplateInfoFile($template)
{
	$template.VSTemplate.WizardExtension.Assembly = "Twia.PowerShell.Wizards, Version=$AppFullVersion, Culture=Neutral, PublicKeyToken=e114232a2dd47c67"
}

Get-ChildItem -Path templates -Filter `*.vstemplate -Recurse | ? DirectoryName -NotMatch '\\([Oo]bj)|([Bb]in)\\' | % {
	Update-XmlFile $_ $function:UpdateVisxTemplateInfoFile
}