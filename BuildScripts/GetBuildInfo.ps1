# Get information about the build.
$BuildBuildNumber = $env:BUILD_BUILDNUMBER
$Version = [Version]($env:PRODUCT_VERSION)
$Company = $env:COMPANY_NAME
$Product = $env:PRODUCT_NAME
$Configuration = $env:BUILDCONFIGURATION
$Platform = $env:BUILDPLATFORM

if(-not $Company)
{
	$Company = "Dani�l te Winkel"
}
if(-not $Product)
{
	$Product = "Twia.PowerShell.VisualStudio"
}
if(-not $Configuration)
{
	$Configuration = "Release"
}
if(-not $Platform)
{
	$Platform = "Any CPU"
}

# Parse the VSTS Build Number to find the version for this build.
if($BuildBuildNumber)
{
	if($BuildBuildNumber -match '^.*(?<version>\d+(\.\d+){2}\.(?<buildnumber>\d+))$')
	{
		$FullVersion = [Version]($Matches['version'])
		$BuildNumber = [int]($Matches['buildnumber'])
	}
	else
	{
		Write-Error "Failed to parse version number from '$BuildBuildNumber'."
		Exit -1
	}
}
else
{
	Write-Error "Could not extract version from build information. Is BUILD_BUILDNUMBER set?"
	Exit -1
}

# Some common information used in builds.
$AppFullVersion = "$($FullVersion.ToString(3)).0"
$copyRightYear = [DateTime]::Now.Year
$copyRight = "Copyright � $copyRightYear, $($Company). All rights reserved."
