# Get information from the build server.
. BuildScripts\GetBuildInfo.ps1

function Update-TextFile($filePath, [ScriptBlock] $updateFunction, $encoding = [System.Text.Encoding]::Unicode)
{
	$file = ($filePath | Resolve-Path -Relative | Resolve-Path).Path
	$text = [System.IO.File]::ReadAllText($file)
	$originalNuspec = $text

	$text = &$updateFunction $text

	if($originalNuspec -ne $text)
	{
		Write-Host "Updating file: '$file'."
		# Remove read-only property from file, as it is read-only on VSTS.
		Set-ItemProperty $file -name IsReadOnly -value $false
		[System.IO.File]::WriteAllText($file, $text, $encoding)
	}
}

function Update-XmlFile($filePath, [ScriptBlock] $updateFunction)
{
	$file = $filePath | Resolve-Path -Relative
	$xmlContent = [Xml](Get-Content $file -Raw)

	$originalxmlContent = $xmlContent.OuterXml

	&$updateFunction $xmlContent

	if($originalxmlContent -ne $xmlContent.OuterXml)
	{
		Write-Host "Updating file: '$file'."
		# Remove read-only property from file, as it is read-only on VSTS.
		Set-ItemProperty $file -name IsReadOnly -value $false
		$absPath = ($file | Resolve-Path).Path
		$xmlContent.Save($absPath)
	}
}


Write-Host "Updating version for '$Product' to version $Version ($FullVersion). Build number: $BuildNumber."

# Update version and other information in appropriate files.
. BuildScripts\UpdateAssemblyInfo.ps1
. BuildScripts\UpdateVisxInfo.ps1
. BuildScripts\UpdateVisxTemplateInfo.ps1
. BuildScripts\UpdateTextFiles.ps1
#. BuildScripts\UpdatePsModuleInfo.ps1
#. BuildScripts\UpdateNuspecInfo.ps1
