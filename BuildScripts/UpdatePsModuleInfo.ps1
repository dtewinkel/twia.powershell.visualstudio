function UpdatePsDataFileLine($moduleInfo, $itemName, $value)
{
	$moduleInfo -replace "$($itemName)\s*=\s*'.*'", "$($itemName) = '$value'"
}

Get-ChildItem -Path . -Filter `*.psd1 -Recurse | ? DirectoryName -NotMatch '\\packages\\' | % {
	Update-TextFile $_ {
		param($moduleInfo)
		$moduleInfo = UpdatePsDataFileLine $moduleInfo "ModuleVersion" $FullVersion
		$moduleInfo = UpdatePsDataFileLine $moduleInfo "CompanyName" $Company
		UpdatePsDataFileLine $moduleInfo "Copyright" $copyRight
	}
}