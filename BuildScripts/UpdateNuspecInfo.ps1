function UpdateNuspecFileLine($nuspec, $itemName, $value)
{
	$nuspec -replace "<$($itemName)>.*</$($itemName)>", "<$($itemName)>$value</$($itemName)>"
}

Get-ChildItem -Path . -Filter `*.nuspec -Recurse | ? DirectoryName -NotMatch '\\packages\\' | % {
	Update-TextFile $_ {
		param($nuspec)
		$nuspec = UpdateNuspecFileLine $nuspec "version" $FullVersion
		$nuspec = UpdateNuspecFileLine $nuspec "owners" $Company
		$nuspec = UpdateNuspecFileLine $nuspec "authors" $Company
		UpdateNuspecFileLine $nuspec "copyright" $copyRight
	}
}