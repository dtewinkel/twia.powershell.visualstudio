function UpdateTextFile($text)
{
	$text -replace "Copyright.*ll rights reserved.", $copyRight
}

$encoding = [System.Text.Encoding]::Utf8

Update-TextFile ReadMe.Md $function:UpdateTextFile $encoding
Update-TextFile deployments\Twia.PowerShell.Vsix\License.Md $function:UpdateTextFile $encoding
