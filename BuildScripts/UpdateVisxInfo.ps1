function UpdateVisxInfoFile($manifest)
{
	$manifest.PackageManifest.Metadata.Identity.Version = "$Version"
	$manifest.PackageManifest.Metadata.Identity.Publisher = $Company
}

Get-ChildItem -Path deployments -Filter `*.vsixmanifest -Recurse | ? DirectoryName -NotMatch '\\([Oo]bj)|([Bb]in)\\' | % {
	Update-XmlFile $_ $function:UpdateVisxInfoFile
}