function UpdateCsAssemblyInfoFileLine($assemblyInfo, $itemName, $value)
{
	$assemblyInfo -replace "\[assembly:\s*$($itemName)\("".*""\)\]", "[assembly: $($itemName)(""$value"")]"
}

function UpdateCsAssemblyInfoFile($assemblyInfo)
{
		$assemblyInfo = UpdateCsAssemblyInfoFileLine $assemblyInfo "AssemblyCopyright" $copyRight
		$assemblyInfo = UpdateCsAssemblyInfoFileLine $assemblyInfo "AssemblyVersion" "$AppFullVersion"
		$assemblyInfo = UpdateCsAssemblyInfoFileLine $assemblyInfo "AssemblyFileVersion" $FullVersion
		$assemblyInfo = UpdateCsAssemblyInfoFileLine $assemblyInfo "AssemblyCompany" $Company
		UpdateCsAssemblyInfoFileLine $assemblyInfo "AssemblyProduct" $Product
}

Get-ChildItem -Path . -Filter AssemblyInfo.cs -Recurse | ? DirectoryName -Match '\\Properties$' | % {
	Update-TextFile $_ $function:UpdateCsAssemblyInfoFile
}