﻿using EnvDTE;

namespace Twia.PowerShell.Wizards
{
    public class PesterWizard : WizardBase
    {
        public override void ProjectFinishedGenerating(Project project)
        {
            SetDebugToScript(project);
            base.ProjectFinishedGenerating(project);
        }
    }
}
