﻿using System.Linq;

namespace Twia.PowerShell.Wizards
{
    public class CmdletInfo
    {
        public CmdletInfo(string verb, string noun)
        {
            Verb = verb;
            Noun = noun;
        }

        public string Verb { get; set; }

        public string Noun { get; set; }

        public string CodeVerb
        {
            get
            {
                var selectedItem = PowerShellHelper.GetSortedVerbItems()
                    .FirstOrDefault(item => item.Verb == Verb);
                if (selectedItem != null)
                {
                    return $"{selectedItem.Class}.{selectedItem.Verb}";
                }
                return $"\"{Verb}\"";
            }
        }

        public string CmdletName => $@"{Verb}-{Noun}";


    }
}