﻿using System.Linq;
using System.Windows;

namespace Twia.PowerShell.Wizards
{
    /// <summary>
    /// Interaction logic for PowerShellCmdletProjectForm.xaml
    /// </summary>
    public partial class PowerShellCmdletProjectForm
    {

        private readonly ProjectViewModel _model;

        public PowerShellCmdletProjectForm()
        {
            _model = new ProjectViewModel
            {
                VerbItems = PowerShellHelper.GetSortedVerbItems(),
                CmdletInfo = new CmdletInfo("New", "Item")
            };
            _model.SelectedItem = _model.VerbItems.FirstOrDefault(item => item.Verb == "New");
            DataContext = _model;
            InitializeComponent();
        }

        public CmdletInfo CmdletInfo => _model.CmdletInfo;

        private void OkButton_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
