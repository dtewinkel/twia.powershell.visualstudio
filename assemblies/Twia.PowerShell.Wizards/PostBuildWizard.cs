﻿using EnvDTE;

namespace Twia.PowerShell.Wizards
{
    public class PostBuildWizard : WizardBase
    {
        public override void ProjectItemFinishedGenerating(ProjectItem projectItem)
        {
            AddBuildEvent(projectItem, "PostBuildEvent");
            base.ProjectItemFinishedGenerating(projectItem);
        }
    }
}