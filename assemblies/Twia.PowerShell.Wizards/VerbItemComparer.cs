﻿using System;
using System.Collections.Generic;

namespace Twia.PowerShell.Wizards
{
    internal class VerbItemComparer : IComparer<VerbItem>
    {
        public int Compare(VerbItem x, VerbItem y)
        {
            return String.CompareOrdinal(x.Verb, y.Verb);
        }
    }
}