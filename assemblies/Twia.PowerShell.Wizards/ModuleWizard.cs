﻿using System.Collections.Generic;
using System.IO;
using EnvDTE;
using Microsoft.VisualStudio.TemplateWizard;

namespace Twia.PowerShell.Wizards
{
    public class ModuleWizard : WizardBase
    {
        public override void RunStarted(object automationObject, Dictionary<string, string> replacementsDictionary, WizardRunKind runKind, object[] customParams)
        {
            if (runKind == WizardRunKind.AsNewProject)
            {
                PowerShellCmdletProjectForm dialog = new PowerShellCmdletProjectForm();
                var result = dialog.ShowDialog();
                if (result.HasValue && result.Value)
                {
                    var cmdletInfo = dialog.CmdletInfo;
                    replacementsDictionary.Add("$verb$", cmdletInfo.Verb);
                    replacementsDictionary.Add("$verb.code$", cmdletInfo.CodeVerb);
                    replacementsDictionary.Add("$noun$", cmdletInfo.Noun);
                }
                else
                {
                    // Clean up the project that was written to disk.
                    string destinationDirectory = replacementsDictionary["$destinationdirectory$"];
                    if (!string.IsNullOrWhiteSpace(destinationDirectory) && Directory.Exists(destinationDirectory))
                    {
                        Directory.Delete(destinationDirectory, true);
                    }

                    throw new WizardCancelledException();
                }
            }
            base.RunStarted(automationObject, replacementsDictionary, runKind, customParams);
        }

        public override void ProjectFinishedGenerating(Project project)
        {
            SetDebugToScript(project);
            base.ProjectFinishedGenerating(project);
        }
    }
}
