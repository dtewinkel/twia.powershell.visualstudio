using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;

namespace Twia.PowerShell.Wizards
{
    internal sealed class ItemViewModel : INotifyPropertyChanged, INotifyDataErrorInfo
    {
        private string _previewText;
        private bool _autoGenerate;
        private VerbItem _selectedItem;
        public List<VerbItem> VerbItems { get; set; }

        public VerbItem SelectedItem
        {
            get
            {
                return _selectedItem;
            }

            set
            {
                if (_selectedItem?.ToString() != value?.ToString())
                {
                    _selectedItem = value;
                    OnPropertyChanged(nameof(SelectedItem));
                    ValidateVerb();
                    SetPreviewText();
                }
            }
        }

        public CmdletInfo CmdletInfo { get; set; }

        public FileInfo FileInfo { get; set; }


        public string Verb
        {
            get
            {
                return CmdletInfo.Verb;
            }

            set
            {
                if (CmdletInfo.Verb != value)
                {
                    CmdletInfo.Verb = value;
                    OnPropertyChanged(nameof(Verb));
                    ValidateVerb();
                    SetPreviewText();
                    FileInfo.SetFromVerbAndNoun(Verb, Noun);
                    OnPropertyChanged(nameof(FileName));
                }
            }
        }

        public string Noun
        {
            get
            {
                return CmdletInfo.Noun;
            }

            set
            {
                if (CmdletInfo.Noun != value)
                {
                    CmdletInfo.Noun = value;
                    OnPropertyChanged(nameof(Noun));
                    ValidateNoun();
                    SetPreviewText();
                    FileInfo.SetFromVerbAndNoun(Verb, Noun);
                    OnPropertyChanged(nameof(FileName));
                }
            }
        }

        public string FileName
        {
            get
            {
                return FileInfo.FileName;
            }

            set
            {
                if (FileInfo.FileName != value)
                {
                    FileInfo.SetFileName(value);
                    ValidateFileName();
                    OnPropertyChanged(nameof(FileName));
                }
            }
        }

        private void ValidateFileName()
        {
        }

        public bool AutoGenerateFileName
        {
            get
            {
                return _autoGenerate;
            }

            set
            {
                if (_autoGenerate != value)
                {
                    _autoGenerate = value;
                    OnPropertyChanged(nameof(AutoGenerateFileName));
                    if (_autoGenerate)
                    {
                        FileInfo.SetFromVerbAndNoun(Verb, Noun);
                    }
                    else
                    {
                        FileInfo.SetFromOriginalFileName();
                    }
                    OnPropertyChanged(nameof(FileName));
                }
            }
        }

        public void ValidateNoun()
        {
            ValidatePowerShellPropertyPart("Noun", CmdletInfo.Noun, "noun");
        }

        public void ValidateVerb()
        {
            ValidatePowerShellPropertyPart("Verb", CmdletInfo.Verb, "verb");
        }

        private void ValidatePowerShellPropertyPart(string propertyName, string part, string partName)
        {
            RemoveErrors(propertyName);
            if (string.IsNullOrWhiteSpace(part))
            {
                AddError(propertyName, $"The {partName} cannot be empty!");
            }
            if (!PowerShellHelper.IsNounOrVerbValid(part))
            {
                AddError(propertyName, $"The {partName} contains illegal characters!");
            }
        }

        public void SetPreviewText()
        {
            PreviewText = CmdletInfo.CmdletName;
        }

        public string PreviewText
        {
            get
            {
                return _previewText;
            }

            set
            {
                if (_previewText != value)
                {
                    _previewText = value;
                    OnPropertyChanged(nameof(PreviewText));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            Debug.WriteLine($"Property '{propertyName}' changed!");
            handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void AddError(string propertyName, string error)
        {
            if (_errors.ContainsKey(propertyName))
            {
                _errors[propertyName].Add(error);
            }
            else
            {
                _errors.TryAdd(propertyName, new List<string> {error});
            }
            OnErrorsChanged(propertyName);
        }

        private void RemoveErrors(string propertyName)
        {
            if (_errors.ContainsKey(propertyName))
            {
                List<string> removed;
                _errors.TryRemove(propertyName, out removed);
                OnErrorsChanged(propertyName);
            }
        }

        readonly ConcurrentDictionary<string, List<string>> _errors = new ConcurrentDictionary<string, List<string>>();

        public IEnumerable GetErrors(string propertyName)
        {
            if (_errors.ContainsKey(propertyName))
            {
                return _errors[propertyName];
            }
            return null;
        }

        public bool HasErrors => _errors.Count > 0;

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        private void OnErrorsChanged(string propertyName)
        {
            var handler = ErrorsChanged;
            handler?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
        }
    }
}