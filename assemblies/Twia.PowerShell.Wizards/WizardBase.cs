﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using EnvDTE;
using Microsoft.VisualStudio.TemplateWizard;

namespace Twia.PowerShell.Wizards
{
    public class WizardBase : IWizard
    {
        public virtual void RunStarted(object automationObject, Dictionary<string, string> replacementsDictionary, WizardRunKind runKind, object[] customParams)
        {
            replacementsDictionary.Add("$powershellEnv$", PowerShellHelper.PowerShellExePathEnv);
            replacementsDictionary.Add("$powershellMacro$", PowerShellHelper.PowerShellExePathMacro);
        }


        public virtual bool ShouldAddProjectItem(string filePath)
        {
            return true;
        }


        public virtual void RunFinished()
        {
        }


        public virtual void BeforeOpeningFile(ProjectItem projectItem)
        {

        }


        public virtual void ProjectItemFinishedGenerating(ProjectItem projectItem)
        {
        }


        public virtual void ProjectFinishedGenerating(Project project)
        {
        }


        protected void SetDebugToScript(Project project, string scriptName = "Start-Debug.ps1", bool exitOnDone = false)
        {
            string noExit = exitOnDone ? "" : "-NoExit ";

            // First make sure we have some settings in the *.csproj.user file.
            foreach (Configuration config in project.ConfigurationManager)
            {
                config.Properties.Item("StartAction").Value = 1; // Launch external program.
                config.Properties.Item("StartProgram").Value =
                    Environment.ExpandEnvironmentVariables(PowerShellHelper.PowerShellExePathEnv);
                config.Properties.Item("StartArguments").Value =
                    $@"{noExit}-NoLogo -ExecutionPolicy RemoteSigned -command "". ..\..\{scriptName}""";
            }
            project.Save();

            // Now put the right value in for StartProgram, as we can't do that through the properties.
            XmlDocument userSettings = new XmlDocument();
            string fileName = project.FileName + ".user";
            userSettings.Load(fileName);
            XmlNamespaceManager namespaceManager = new XmlNamespaceManager(userSettings.NameTable);
            namespaceManager.AddNamespace("ns", "http://schemas.microsoft.com/developer/msbuild/2003");
            XmlElement root = userSettings.DocumentElement;
            if (root != null)
            {
                XmlNodeList nodes = root.SelectNodes("//ns:StartProgram", namespaceManager);

                if (nodes != null && nodes.Count > 0)
                {
                    IEnumerator enumerator = nodes.GetEnumerator();

                    while (enumerator != null && enumerator.MoveNext())
                    {
                        XmlNode node = enumerator.Current as XmlNode;
                        if (node != null)
                        {
                            node.InnerText = PowerShellHelper.PowerShellExePathMacro;
                        }
                    }
                    userSettings.Save(fileName);
                }
            }
        }

        public void AddBuildEvent(ProjectItem projectItem, string itemName)
        {
            Project project = projectItem.ContainingProject;

            StringBuilder buildEvent = new StringBuilder(project.Properties.Item(itemName).Value as string);
            if (!string.IsNullOrWhiteSpace(buildEvent.ToString()))
            {
                buildEvent.AppendLine();
            }
            buildEvent.Append(PowerShellHelper.PowerShellExePathEnv);
            buildEvent.Append(" -NonInteractive");
            buildEvent.Append(" -NoProfile");
            buildEvent.Append(" -ExecutionPolicy RemoteSigned");
            buildEvent.Append(" -NoLogo");
            buildEvent.Append(" -Command ");
            buildEvent.Append('"');
            buildEvent.Append($"& '$(ProjectDir){projectItem.Name}'");
            buildEvent.Append(" -ConfigurationName '$(ConfigurationName)'");
            buildEvent.Append(" -OutDir '$(OutDir)'");
            buildEvent.Append(" -DevEnvDir '$(DevEnvDir)'");
            buildEvent.Append(" -PlatformName '$(PlatformName)'");
            buildEvent.Append(" -ProjectDir '$(ProjectDir)'");
            buildEvent.Append(" -ProjectPath '$(ProjectPath)'");
            buildEvent.Append(" -ProjectName '$(ProjectName)'");
            buildEvent.Append(" -ProjectFileName '$(ProjectFileName)'");
            buildEvent.Append(" -ProjectExt '$(ProjectExt)'");
            buildEvent.Append(" -SolutionDir '$(SolutionDir)'");
            buildEvent.Append(" -SolutionPath '$(SolutionPath)'");
            buildEvent.Append(" -SolutionName '$(SolutionName)'");
            buildEvent.Append(" -SolutionFileName '$(SolutionFileName)'");
            buildEvent.Append(" -SolutionExt '$(SolutionExt)'");
            buildEvent.Append(" -TargetDir '$(TargetDir)'");
            buildEvent.Append(" -TargetPath '$(TargetPath)'");
            buildEvent.Append(" -TargetName '$(TargetName)'");
            buildEvent.Append(" -TargetFileName '$(TargetFileName)'");
            buildEvent.Append(" -TargetExt '$(TargetExt)'");
            buildEvent.Append('"');
            project.Properties.Item(itemName).Value = buildEvent.ToString();
        }

        public void SetCopyToOutputIfNewer(ProjectItem projectItem)
        {
            projectItem.Properties.Item("CopyToOutputDirectory").Value = 2; // Copy if newer.
        }
    }


}