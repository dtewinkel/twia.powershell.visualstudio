﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Twia.PowerShell.Wizards
{
    /// <summary>
    /// Interaction logic for PowerShellCmdletProjectForm.xaml
    /// </summary>
    public partial class PowerShellCmdletItemForm
    {

        private readonly ItemViewModel _model;

        public PowerShellCmdletItemForm(string fileName, string fileNameFormat)
        {
            _model = new ItemViewModel
            {
                VerbItems = PowerShellHelper.GetSortedVerbItems(),
                CmdletInfo = new CmdletInfo("New", "Item"),
                FileInfo = new FileInfo(fileName, fileNameFormat),
                AutoGenerateFileName = true
            };
            _model.SelectedItem = _model.VerbItems.FirstOrDefault(item => item.Verb == "New");
            DataContext = _model;
            InitializeComponent();
        }

        public CmdletInfo CmdletInfo => _model.CmdletInfo;

        public FileInfo FileInfo => _model.FileInfo;


        private void OkButton_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
