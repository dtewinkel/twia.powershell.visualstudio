﻿using System.Linq;

namespace Twia.PowerShell.Wizards
{
    public class FileInfo
    {
        private readonly string _fileNameFormat;
        private readonly string _originalFileName;
        private readonly string _extension;

        public FileInfo(string fileName, string fileNameFormat)
        {
            var file = new System.IO.FileInfo(fileName);
            _originalFileName = fileName;
            _extension = file.Extension;
            FileName = fileName;
            _fileNameFormat = fileNameFormat;
        }

        public void SetFileName(string fileName)
        {
            FileName = fileName;
        }

        public void SetFromVerbAndNoun(string verb, string noun)
        {
            FileName = string.Format(_fileNameFormat, verb, noun, _extension);
        }

        public void SetFromOriginalFileName()
        {
            FileName = _originalFileName;
        }

        public string FileName { get; set; }
    }
}