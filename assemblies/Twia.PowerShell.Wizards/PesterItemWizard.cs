﻿namespace Twia.PowerShell.Wizards
{
    public class PesterItemWizard : WizardBase
    {
        public override void ProjectItemFinishedGenerating(EnvDTE.ProjectItem projectItem)
        {
            SetCopyToOutputIfNewer(projectItem);
            base.ProjectItemFinishedGenerating(projectItem);
        }

    }
}
