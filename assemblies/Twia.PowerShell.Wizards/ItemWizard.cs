﻿using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Microsoft.VisualStudio.TemplateWizard;

namespace Twia.PowerShell.Wizards
{
    public abstract class ItemWizard : WizardBase
    {
        private readonly string _fileNameFormat;

        protected ItemWizard(string fileNameFormat)
        {
            _fileNameFormat = fileNameFormat;
        }


        public override void RunStarted(object automationObject, Dictionary<string, string> replacementsDictionary, WizardRunKind runKind, object[] customParams)
        {
            string destinationFile = replacementsDictionary["$rootname$"];
            if (runKind == WizardRunKind.AsNewItem)
            {
                PowerShellCmdletItemForm dialog = new PowerShellCmdletItemForm(destinationFile, _fileNameFormat);
                var result = dialog.ShowDialog();
                if (result.HasValue && result.Value)
                {
                    var cmdletInfo = dialog.CmdletInfo;
                    replacementsDictionary.Add("$verb$", cmdletInfo.Verb);
                    replacementsDictionary.Add("$verb.code$", cmdletInfo.CodeVerb);
                    replacementsDictionary.Add("$noun$", cmdletInfo.Noun);
                    var fileInfo = dialog.FileInfo;
                     replacementsDictionary.Add("$fileName$", fileInfo.FileName);
                }
                else
                {
                    // Clean up the project that was written to disk.
                    if (!string.IsNullOrWhiteSpace(destinationFile) && File.Exists(destinationFile))
                    {
                        File.Delete(destinationFile);
                    }

                    throw new WizardCancelledException();
                }
            }
            base.RunStarted(automationObject, replacementsDictionary, runKind, customParams);
        }
    }
}
