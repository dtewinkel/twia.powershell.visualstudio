﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Reflection;

namespace Twia.PowerShell.Wizards
{
    class PowerShellHelper
    {
        public static string PowerShellExePathEnv { get; private set; }
        public static string PowerShellExePathMacro { get; private set; }


        static PowerShellHelper()
        {
            const string powerShellExePathRelative = @"\System32\WindowsPowerShell\v1.0\powershell.exe";
            PowerShellExePathEnv = "%SystemRoot%" + powerShellExePathRelative;
            PowerShellExePathMacro = @"$(SystemRoot)" + powerShellExePathRelative;
        }

        public static bool IsNounOrVerbValid(string nounOrVerb)
        {
            return ValidateContainsOnlyAllowedCharacters(nounOrVerb);
        }


        private static bool ValidateContainsOnlyAllowedCharacters(string text)
        {
            const string notAllowed = "#,()[]{}&-/\\$^;:\"'<>|?@`*%+=~\t \n\r";
            return text.IndexOfAny(notAllowed.ToCharArray()) < 0;
        }


        public static List<VerbItem> GetSortedVerbItems()
        {
            List<VerbItem> verbs = new List<VerbItem>();
            verbs.AddRange(GetNames(typeof(VerbsCommon)));
            verbs.AddRange(GetNames(typeof(VerbsCommunications)));
            verbs.AddRange(GetNames(typeof(VerbsData)));
            verbs.AddRange(GetNames(typeof(VerbsDiagnostic)));
            verbs.AddRange(GetNames(typeof(VerbsLifecycle)));
            verbs.AddRange(GetNames(typeof(VerbsOther)));
            verbs.AddRange(GetNames(typeof(VerbsSecurity)));
            verbs.Sort(new VerbItemComparer());
            return verbs;
        }


        private static IEnumerable<VerbItem> GetNames(Type type)
        {
            string typeName = type.Name;
            FieldInfo[] fields = type.GetFields(BindingFlags.Static | BindingFlags.GetField | BindingFlags.Public);
            return fields.Select(fieldInfo => new VerbItem
            {
                Verb = fieldInfo.GetRawConstantValue().ToString(), Class = typeName
            }).ToList();
        }
    }
}
