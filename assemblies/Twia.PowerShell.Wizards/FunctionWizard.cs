﻿namespace Twia.PowerShell.Wizards
{
    class FunctionWizard : ItemWizard
    {
        public FunctionWizard()
            : base("{0}-{1}{2}")
        {
        }

        public override void ProjectItemFinishedGenerating(EnvDTE.ProjectItem projectItem)
        {
            SetCopyToOutputIfNewer(projectItem);
            base.ProjectItemFinishedGenerating(projectItem);
        }
    }
}
