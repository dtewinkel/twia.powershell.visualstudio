﻿using EnvDTE;

namespace Twia.PowerShell.Wizards
{
    public class PreBuildWizard : WizardBase
    {
        public override void ProjectItemFinishedGenerating(ProjectItem projectItem)
        {
            AddBuildEvent(projectItem, "PreBuildEvent");
            base.ProjectItemFinishedGenerating(projectItem);
        }
    }
}