﻿namespace Twia.PowerShell.Wizards
{
    internal class VerbItem
    {
        public string Class { get; set; }

        public string Verb { get; set; }

        public override string ToString()
        {
            return Verb;
        }
    }
}