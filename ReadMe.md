﻿# TWIA PowerShell extension for Visual Studio

This project contains Visual Studio project templates and item templates for PowerShell modules for use in Visual Studio 2015, 2017 and 2019.

For full information about the project, how it is distributed and much more, see the project's [Wiki][] pages. 

# The templates

For full information about the templates, see the project's [Wiki][] pages.

## Project Templates
 
### PowerShell Module project template

The PowerShell module project template creates a project for a script module.

### PowerShell Module With DLL project template

The PowerShell module project template creates a project for a script module and a DLL for C# Cmdlets.

### PowerShell Pester Tests project template

A PowerShell project template for testing PowerShell code in a TDD or BDD fashion using Pester.

## Item Templates
 
### PowerShell C# Cmdlet item template

A C# Cmdlet for a PowerShell Module DLL.

### PowerShell Script Function item template

PowerShell script file for a PowerShell script function providing a template for a full-fledged function. 

### PowerShell pre-build and post-built event item templates

Adds a PowerShell script to the pre-build or post-build event.

### PowerShell Pester test fixture item templates

Adds a Pester test fixture PowerShell script.

# Copyright and License

**Copyright © 2014-2019, Daniël te Winkel (TWIA). All rights reserved.**

See the [License page][] for information on copyright and licenses for this project and for Open Source licenses used in this project.

[NuGet]: http://www.nuget.org/ "NuGet Package Manager"
[GIT]: http://git-scm.com/ "Git distributed source control system"
[Bitbucket]: https://bitbucket.org/ "Hosting of Git and Mercurial repositories"
[JetBrains]: http://www.jetbrains.com
[ReSharper]: http://www.jetbrains.com/resharper/
[Twia.PowerShell.VisualStudio]: https://bitbucket.org/dtewinkel/twia.powershell.visualstudio
[Wiki]: https://bitbucket.org/dtewinkel/twia.powershell.visualstudio/wiki/Home "Wiki home"
[License page]: https://bitbucket.org/dtewinkel/twia.powershell.visualstudio/wiki/License.md "License page"