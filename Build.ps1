﻿<#
    This script can be used to build the solution and run the unit tests locally on the developer's machine.
#>

[CmdletBinding(ConfirmImpact = 'Medium', SupportsShouldProcess)]
param(
    [Parameter()] $BuildConfiguration = "Release",
    [Parameter()] $BuildPlatform = "Any CPU",
    [Parameter()] $Solution = "Twia.PowerShell.VisualStudio.sln",
    [Parameter()] $ProductVersion = "0.0.0",
    [Parameter()] $BuildNumber = "1",
    [Parameter()] [Switch] $RunScripts,
    [Parameter()] [Switch] $Clean,
    [Parameter()] [Switch] $CleanPackages,
    [Parameter()] [Switch] $RunTests,
    [Parameter()] [Switch] $NoBuild
)

# Set the build label for the pre-build script to use.
$Env:PRODUCT_VERSION = $ProductVersion
$Env:BUILD_BUILDNUMBER = "VirtualVaults.PowerShell-CI_$ProductVersion.$BuildNumber"
$Env:BUILDCONFIGURATION = $BuildConfiguration
$Env:BUILDPLATFORM = $BuildPlatform


$start = [DateTime]::Now
$line = '=' * 100

function Write-Time()
{
    $end = [DateTime]::Now
    Write-Host "Running time: $($end - $start)."
    Write-Host $line
}

if($Clean.IsPresent -and $NoBuild.IsPresent -and $RunTests.IsPresent)
{
    Write-Error "Invalid combination of parameters. Cannot clean, not build and run tests!"
    exit -1
}

$msbuildLoc = Get-Command msbuild.exe -ErrorAction SilentlyContinue
if (-not $msbuildLoc)
{
    Write-Host "MsBuild not found in path. Falling back to MSBuild 14.0 (VS2015) path."
    $msBuild = "${env:ProgramFiles(x86)}\MSBuild\14.0\Bin\MSBuild.exe"
}
else
{
    $msBuild = $msbuildLoc.Source
}
if (-not (Test-Path $msBuild))
{
    Write-Error Write-Host "MsBuild not found in path and not in fall-back to MSBuild 14.0 (VS2015) path!"
    exit -1
}

$commonMsbuildOptions = "/m",
			$Solution,
			"/nologo",
			"/p:Configuration=$BuildConfiguration",
			"/p:Platform=$BuildPlatform"

Write-Time

if($Clean.IsPresent)
{
    # Really clean the solution and other stuff so we can do a clean build.
    if($CleanPackages.IsPresent)
    {
        del packages -Force -Recurse -ErrorAction SilentlyContinue
    }
    del Output -Force -Recurse -ErrorAction SilentlyContinue
    del TestResults -Force -Recurse -ErrorAction SilentlyContinue
	del ReadMe.html -Force -ErrorAction SilentlyContinue
	del deployments\Twia.PowerShell.Vsix\Package\Description.html -Force -ErrorAction SilentlyContinue
	del deployments\Twia.PowerShell.Vsix\Resources\License.rtf -Force -ErrorAction SilentlyContinue
    dir obj -Directory -Recurse | select FullName | % { rm $_.FullName -Force -Recurse }
    dir bin -Directory -Recurse | select FullName | % { rm $_.FullName -Force -Recurse }
    # Clean our own administration files.
    Write-Time

    if($PSCmdlet.ShouldProcess("clean", "Run MsBuild"))
    {
		$msBuildOptions = $commonMsbuildOptions
		$msBuildOptions += "/verbosity:q", "/t:clean"
        . $msBuild @msBuildOptions
        Write-Time
    }
}

if(-not $NoBuild.IsPresent)
{
	$nugetExe = "Buildscripts\NuGet\NuGet.exe"
    $nuGetOptions = "restore", $Solution, "-NonInteractive"
    . $nugetExe @nugetOptions
    Write-Time

	if($RunScripts.IsPresent)
	{
		.\BuildScripts\UpdateVersionsInFiles.ps1
	    Write-Time
	}

    Write-Host "Building configuration '$BuildConfiguration' for platform '$BuildPlatform'."
    if($PSCmdlet.ShouldProcess("build", "Run MsBuild"))
    {
		$msBuildOptions = $commonMsbuildOptions
		$msBuildOptions += "/verbosity:m"
        . $msBuild @msBuildOptions
    }
    Write-Time

	if($RunScripts.IsPresent)
	{
		.\BuildScripts\ComposeArtifactsPackage.ps1
	    Write-Time
	}
}

if($RunTests.IsPresent)
{
	$TestExePath = (resolve-path "$env:VS140COMNTOOLS\..\IDE\CommonExtensions\Microsoft\TestWindow")
	$testExe = Join-Path $TestExePath.Path VSTest.Console.exe
	$files = dir .\tests\*Test*\bin\$BuildConfiguration\*test*.dll
	$testOptions = "/Enablecodecoverage",
	               $files,
	               "/Platform:x64",
	               "/Logger:trx"

    # ToDo: Add test-run similar to run on TFS server.
	. $testExe @testOptions
    Write-Time
}

$end = [DateTime]::Now

Write-Host "Build.ps1 ran for: $($end - $start)."