This file contains additional information that must be entered on the [Visual Studio Gallery](http://visualstudiogallery.msdn.microsoft.com/5a41931c-da0e-4675-a461-108ed6f1f2ee).

## Tags

PowerShell,item,project,template,module,script,pester,tdd,bdd

## Description

The TWIA PowerShell extension provides templates for PowerShell Module projects and items.

### Templates

It contains project templates:

* The 'PowerShell Module' project template.
* The 'PowerShell Module With DLL' project template.
* The 'PowerShell Pester Tests' project template.

It contains the following item templates:

* The 'PowerShell C# Cmdlet' item template.
* The 'PowerShell Script Function' item template.
* The 'PowerShell pre-build event' item template.
* The 'PowerShell post-build event' item template.
* The 'PowerShell Pester Test Fixture' item template.

See the [project site](https://bitbucket.org/dtewinkel/twia.powershell.visualstudio) for more information.

### Recent changes

In this release the following changes have been made:

* Make this extention available for Visual Studio 2019.
* Remove support for Visual Studio 2012 and 2013. 

For older changes see the [Revision History](https://bitbucket.org/dtewinkel/twia.powershell.visualstudio/wiki/RevisionHistory).

### Known issues

In PowerShell 4 relative paths in $PSModulePath are not allowed anymore. The local path must be added as an absolute path.

For existing projects made with this extension before version 1.4.1, the following fix can be applied:

In file Start-Debug.ps1 change line 16:

```posh
$env:PSModulePath = ".;$env:PSModulePath"
```
to

```posh
$env:PSModulePath = (Resolve-Path .).Path + ";" + $env:PSModulePath
```
### Feedback

Please let me know what you think about this extension by writing a review or start a discussion in the Q and A section.
